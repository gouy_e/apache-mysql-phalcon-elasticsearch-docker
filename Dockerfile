FROM centos:centos7

MAINTAINER gouy_e

RUN yum -y update

RUN yum -y install httpd httpd-mmn gcc make git mariadb mariadb-server  python-setuptools java-1.8.0-openjdk wget tar unzip which
ADD default.conf /etc/httpd/conf.d/virtualhost-default.conf
RUN echo "bind-address=0.0.0.0" >> /etc/my.cnf
RUN mysql_install_db

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
RUN rpm -Uvh http://mirrors.karan.org/epel7/Packages/re2c/20131231011915/0.13.5-1.el6.x86_64/re2c-0.13.5-1.el7.x86_64.rpm
RUN yum -y install php php-opcache php-devel php-mbstring php-mcrypt php-mysqlnd php-pecl-xdebug php-pecl-xhprof pcre-devel php-gd php-gmp

RUN git clone --depth=1 git://github.com/phalcon/cphalcon.git /root/phalcon
RUN cd /root/phalcon/build && ./install 64bits
RUN echo extension=/usr/lib64/php/modules/phalcon.so >> /etc/php.d/phalcon.ini

ENV ES_PKG_NAME elasticsearch-1.6.0

RUN \
  cd / && \
  wget https://download.elasticsearch.org/elasticsearch/elasticsearch/$ES_PKG_NAME.tar.gz && \
  tar xvzf $ES_PKG_NAME.tar.gz && \
  rm -f $ES_PKG_NAME.tar.gz && \
  mv /$ES_PKG_NAME /usr/share/elasticsearch

RUN /usr/share/elasticsearch/bin/plugin -install river-jdbc -url http://xbib.org/repository/org/xbib/elasticsearch/importer/elasticsearch-jdbc/1.6.0.0/elasticsearch-jdbc-1.6.0.0-dist.zip

RUN easy_install supervisor
ADD supervisord.conf /etc/supervisord.conf

EXPOSE 80
EXPOSE 3306
EXPOSE 9200

CMD ["/usr/bin/supervisord"]
